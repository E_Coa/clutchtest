package coa.test.glutchtest.model;

public class ContentModel
        extends BaseModel {

    @SuppressWarnings("membername")
    public String _content; // NOPMD

    @Override
    public String toString() {
        return _content;
    }

}