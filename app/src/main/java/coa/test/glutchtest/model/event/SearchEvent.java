package coa.test.glutchtest.model.event;

import coa.test.glutchtest.model.PhotoModel;

import java.util.List;

public class SearchEvent
        extends BaseServiceEvent<List<PhotoModel>> {

    public SearchEvent(List<PhotoModel> item, Throwable exception) {
        super(item, exception);
    }

}