package coa.test.glutchtest.model;

public class OwnerModel
        extends BaseModel {

    public String username;

    @Override
    public String toString() {
        return username;
    }

}