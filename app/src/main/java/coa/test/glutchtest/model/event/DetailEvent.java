package coa.test.glutchtest.model.event;

import coa.test.glutchtest.model.PhotoInfoModel;

public class DetailEvent
        extends BaseServiceEvent<PhotoInfoModel> {

    public DetailEvent(PhotoInfoModel item, Throwable exception) {
        super(item, exception);
    }

}