package coa.test.glutchtest.util;

/**
 * Created by Elvis on 2/9/2017.
 */

public interface RowClickListener<T> {

    void onRowClicked(int row, T item);

}
