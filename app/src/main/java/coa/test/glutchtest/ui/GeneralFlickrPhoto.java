package coa.test.glutchtest.ui;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import coa.test.glutchtest.R;
import coa.test.glutchtest.adapter.Decoration.PhotoGridDecoration;
import coa.test.glutchtest.adapter.PhotoAdapter;
import coa.test.glutchtest.model.PhotoModel;
import coa.test.glutchtest.model.event.SearchEvent;
import coa.test.glutchtest.service.FlickrService;
import coa.test.glutchtest.service.LocationTrackService;
import coa.test.glutchtest.util.AppUtil;
import coa.test.glutchtest.util.RowClickListener;
import coa.test.glutchtest.util.ScreenStateManager;
import timber.log.Timber;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/**
 * Created by Elvis on 2/9/2017.
 */

public class GeneralFlickrPhoto extends AbstractBaseActivity implements
        RowClickListener<PhotoModel>,
        SwipeRefreshLayout.OnRefreshListener {

    public static final String mBroadcastLocationAction = "coa.test.glutchtest.serice.LocationTrackService";
    private IntentFilter intentFilter;
    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();
    private String lastLatitudeKnow="", lastLongitudeKnow="";
    private final static int ALL_PERMISSIONS_RESULT = 101;
    private int page = 1;
    private boolean isLoading;
    private final FlickrService flickrService = FlickrService.INSTANCE;
    private PhotoAdapter adapter;
    private ScreenStateManager screenStateManager;
    private boolean serviceOn=true;
    private Menu menu;

    @BindView(R.id.toolbar) protected Toolbar toolbar;
    @BindView(R.id.swipe) protected SwipeRefreshLayout swipe;
    @BindView(R.id.linear) protected LinearLayout linear;
    @BindView(R.id.recycler) protected RecyclerView recycler;

    @Override
    public int getLayout() {
        return R.layout.activity_general_photo;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
        intentFilter = new IntentFilter();
        intentFilter.addAction(mBroadcastLocationAction);
        swipe.setColorSchemeColors(ContextCompat.getColor(this, R.color.colorAccent));
        swipe.setOnRefreshListener(this);
        adapter = new PhotoAdapter();
        adapter.setRowClickListener(this);
        recycler.setAdapter(adapter);
        recycler.setLayoutManager(new GridLayoutManager(this, 2));
        recycler.addItemDecoration(new PhotoGridDecoration(2, AppUtil.dpToPx(10,this), true));
        recycler.setItemAnimator(new DefaultItemAnimator());
        setScrollListener();
        screenStateManager = new ScreenStateManager(linear);
        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);
        permissionsToRequest = findUnAskedPermissions(permissions);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0)
                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }
        Intent intent = new Intent(getApplicationContext(), LocationTrackService.class);
        startService(intent);
        //sendRequest();
    }

    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();

        for (Object perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(Object permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission((String) permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (Object perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale((String) permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions((String[]) permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(GeneralFlickrPhoto.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(mReceiver, intentFilter);
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
             if (intent.getAction().equals(mBroadcastLocationAction)) {
                 Log.d("Location Receiver","Longitude:" + Double.toString(intent.getDoubleExtra("longitude",0.0)) + "\nLatitude:" + Double.toString(intent.getDoubleExtra("latitude",0.0)));
                 lastLatitudeKnow = Double.toString(intent.getDoubleExtra("latitude",10.5168109));
                 lastLongitudeKnow = Double.toString(intent.getDoubleExtra("longitude",-66.9539265));
                 if (serviceOn)
                    sendRequest(Double.toString(intent.getDoubleExtra("longitude",-66.9539265)),Double.toString(intent.getDoubleExtra("latitude",10.5168109)));
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent stopIntent = new Intent(GeneralFlickrPhoto.this,
                LocationTrackService.class);
        stopService(stopIntent);
    }
    @Override
    public void onRefresh() {
        page = 1;
        if (!lastLongitudeKnow.isEmpty() && !lastLatitudeKnow.isEmpty() && serviceOn)
            sendRequest(lastLongitudeKnow,lastLatitudeKnow);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(SearchEvent event) {
        isLoading = false;
        // fired by pull to refresh
        if (swipe.isRefreshing()) {
            swipe.setRefreshing(false);
            adapter.clear();
        }
        if (isScreenEmpty()) {
            if (event.exception != null) {
                screenStateManager.showError(R.string.errorMessage);
            } else if (AppUtil.isNullOrEmpty(event.item)) {
                screenStateManager.showEmpty(R.string.emptyText);
            } else {
                screenStateManager.hideAll();
                adapter.addAll(event.item);
            }
        } else {
            adapter.remove(adapter.getItemCount() - 1); //remove progress item
            if (event.exception != null) {
                showSnack(R.string.errorMessage);
            } else if (AppUtil.isNullOrEmpty(event.item)) {
                showSnack(R.string.emptyText);
            } else {
                adapter.addAll(event.item);
            }
        }
    }

    private void sendRequest(String longitude, String latitude) {
        Timber.i("sendRequest: " + page);
        if (AppUtil.isConnected()) {
            isLoading = true;
            flickrService.searchAsync(page++,longitude,latitude);
            if (isScreenEmpty()) screenStateManager.showLoading();
            else adapter.addAll(null); // add null , so the adapter will check view_type and show progress bar at bottom
        } else {
            swipe.setRefreshing(false);
            if (isScreenEmpty()) screenStateManager.showConnectionError();
            else showConnectionError();
        }
    }

    private void setScrollListener() {
        recycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
                if (!isLoading && (visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0 && totalItemCount >= FlickrService.PAGE_SIZE) {
                    if (!lastLongitudeKnow.isEmpty() && !lastLatitudeKnow.isEmpty() && serviceOn)
                        sendRequest(lastLongitudeKnow,lastLatitudeKnow);
                }
            }
        });
    }

    private boolean isScreenEmpty() {
        return adapter == null || adapter.getItemCount() == 0;
    }


    @Override
    public void onRowClicked(int row, PhotoModel item) {
        startActivity(DetailFlickrPhoto.createIntent(this, row, adapter.getAll()));
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        this.menu = menu;
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_service:
                MenuItem menuItem = menu.findItem(R.id.action_service);
                if (serviceOn){
                    menuItem.setTitle(R.string.action_resume_service);
                    Intent stopIntent = new Intent(GeneralFlickrPhoto.this,
                            LocationTrackService.class);
                    stopService(stopIntent);
                    Toast.makeText(GeneralFlickrPhoto.this,R.string.action_st_service,Toast.LENGTH_SHORT);
                    serviceOn = false;
                } else {
                    menuItem.setTitle(R.string.action_stop_service);
                    Intent intent = new Intent(getApplicationContext(), LocationTrackService.class);
                    startService(intent);
                    Toast.makeText(GeneralFlickrPhoto.this,R.string.action_res_service,Toast.LENGTH_SHORT);
                    serviceOn = true;
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
