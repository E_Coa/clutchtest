package coa.test.glutchtest.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.OnPageChange;
import coa.test.glutchtest.R;
import coa.test.glutchtest.adapter.PhotoDetailAdapter;
import coa.test.glutchtest.model.PhotoModel;
import coa.test.glutchtest.model.event.ClickEvent;
import coa.test.glutchtest.model.event.DetailEvent;
import coa.test.glutchtest.service.FlickrService;
import coa.test.glutchtest.util.AppUtil;
import coa.test.glutchtest.util.ParallaxPageTransformer;
import timber.log.Timber;

public class DetailFlickrPhoto
        extends AbstractBaseActivity {

    private static final String EXTRA_INDEX = "EXTRA_INDEX";
    private static final String EXTRA_ITEMS = "EXTRA_ITEMS";
    private static final String ERROR_LITERAL = "-";

    private List<PhotoModel> items;
    private final FlickrService flickrService = FlickrService.INSTANCE;
    private DetailEvent detailEvent;

    @BindView(R.id.toolbar) protected Toolbar toolbar;
    @BindView(R.id.pager) protected ViewPager pager;
    @BindView(R.id.tvTitle) protected TextView tvTitle;
    @BindView(R.id.tvDate) protected TextView tvDate;
    @BindView(R.id.lnrFooter) protected View lnrFooter;

    public static Intent createIntent(Context context, int index, List<PhotoModel> items) {
        return new Intent(context, DetailFlickrPhoto.class)
                .putExtra(EXTRA_INDEX, index)
                .putExtra(EXTRA_ITEMS, (Serializable) items);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_detail_photo;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        int index = getIntent().getIntExtra(EXTRA_INDEX, -1);
        items = (List<PhotoModel>) getIntent().getSerializableExtra(EXTRA_ITEMS);
        if (index == -1) {
            finish();
        } else if (!AppUtil.isConnected()) {
            showConnectionError();
        } else {
            pager.setPageTransformer(false, new ParallaxPageTransformer(R.id.image));
            pager.setAdapter(new PhotoDetailAdapter(getSupportFragmentManager(), items));
            pager.setCurrentItem(index);
            onPageSelected(index);
        }

    }

    @OnPageChange(R.id.pager)
    protected void onPageSelected(int position) {
        toolbar.setTitle(R.string.loading);
        tvTitle.setText(R.string.loading);
        tvDate.setText(R.string.loading);
        flickrService.getDetailAsync(items.get(position).id);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onServiceEvent(DetailEvent event) {
        detailEvent = event;
        if (event.exception == null) {
            toolbar.setTitle(event.item.owner.toString());
            tvTitle.setText(event.item.title.toString());
            tvDate.setText(event.item.getFormattedDate());
        } else {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setIcon(R.mipmap.ic_launcher);
            tvTitle.setText(ERROR_LITERAL);
            tvDate.setText(ERROR_LITERAL);
            showSnack(R.string.errorMessage);
        }
    }

    // fired by child fragment
    // child's photoView absorbs touch event so parent's touch events not fired
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onClickEvent(ClickEvent event) {
        int value = lnrFooter.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE;
        Timber.i("pagerClick: " + value);
        lnrFooter.setVisibility(value);
    }

}