package coa.test.glutchtest.adapter.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

public class ProgressViewHolder
        extends RecyclerView.ViewHolder {

    public ProgressViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

}