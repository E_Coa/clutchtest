package coa.test.glutchtest.adapter.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import coa.test.glutchtest.R;

public class PhotoViewHolder
        extends RecyclerView.ViewHolder {

    @BindView(R.id.image)
    public ImageView image;

    public PhotoViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

}