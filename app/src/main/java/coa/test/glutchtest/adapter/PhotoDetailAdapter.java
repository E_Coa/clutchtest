package coa.test.glutchtest.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import coa.test.glutchtest.model.PhotoModel;
import coa.test.glutchtest.ui.DetailZoomeablePhotoFragment;

/**
 * Created by Elvis on 2/9/2017.
 */

public  class PhotoDetailAdapter
        extends FragmentStatePagerAdapter {

    private final List<PhotoModel> items;

    public PhotoDetailAdapter(FragmentManager fm, List<PhotoModel> items) {
        super(fm);
        this.items = items;
    }

    @Override
    public Fragment getItem(int position) {
        return DetailZoomeablePhotoFragment.newInstance(items.get(position));
    }

    @Override
    public int getCount() {
        return items.size();
    }

}